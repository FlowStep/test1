#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

struct factorizedfact
{
	bool *isprime;
	int *freq;
};

struct factorizedfact itoff(int n);
void writeff(struct factorizedfact *a);
struct factorizedfact divide(struct factorizedfact a, struct factorizedfact b);
int fftoi(struct factorizedfact a);
bool isdegree(int a, int b);

int main()
{
	int n = 0;
	struct factorizedfact a, b;
	scanf("%d", &n); a = itoff(n);
	writeff(&a);
	scanf("%d", &n); b = itoff(n);
	writeff(&b);
	a = divide(a, b);
	writeff(&a);
	printf("%d\n", fftoi(a));
	system("pause");
}

struct factorizedfact itoff(int n)
{
	struct factorizedfact a; 
	a.isprime = malloc(sizeof(bool) * n);
	a.freq = malloc(sizeof(int) * n);
	for (int i = 2; i <= n; ++i)
	{
		a.isprime[i] = true;
		a.freq[i] = 0;
	}
	a.freq[0] = n; a.freq[1] = 0;
	a.isprime[0] = false; a.isprime[1] = false;

	for (int i = 2; i <= n; ++i)
		if (a.isprime[i])
		{
			a.freq[i] += n / i;
			for (int j = i*i; j <= n; j += i)
			{
				if (isdegree(j, i))
					a.freq[i] += n / j;
				a.isprime[j] = false;
			}
		}

	return a;
}

void writeff(struct factorizedfact *a)
{
	for (int i = 0; i <= (*a).freq[0]; ++i)
		printf("%d %d %d\n", i, (*a).isprime[i], (*a).freq[i]);
	putchar('\n');
}

struct factorizedfact divide(struct factorizedfact a, struct factorizedfact b)
{
	for (int i = 1; i <= b.freq[0]; ++i)
		a.freq[i] -= b.freq[i];
	return a;
}

int fftoi(struct factorizedfact a)
{
	int n = 1;
	for (int i = 2; i <= a.freq[0]; ++i)
	{
		if (a.freq[i] != 0);
		n *= pow(i, a.freq[i]);
	}
	return n;
}

bool isdegree(int a, int b)
{
	if (a % b != 0)
		return false;
	if (a / b == 1)
		return true;
	
	isdegree(a / b, b);
}
